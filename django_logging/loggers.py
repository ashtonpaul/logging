import logging
try:
    from Queue import Queue  # Python 2.x
except:
    from queue import Queue  # Python 3.x
from threading import Thread

from django.utils.timezone import now
from django.conf import settings

MIDDLEWARE_VERBOSE = 'LoggingMiddleware'
DISALLOWED_USER_AGENTS = ['']

# Log processing queue
log_queue = Queue()

# Max Number of threads used for logging
num_log_threads = 2

# Instantiate logger
logger = logging.getLogger(MIDDLEWARE_VERBOSE)


class LogEntry(object):
    """
    Log entry object for each log line
    """
    pass

    def __init__(self, request):
        ip = (request.META.get('HTTP_X_FORWARDED_FOR') or
              request.META.get('REMOTE_ADDR'))

        self.timestamp = now()
        self.path = request.path
        self.method = request.method
        self.params = request.GET.dict()
        self.ip = '{}'.format(ip).split(", ")[0]

    def __str__(self):
        return '[{}] {} {}'.format(self.timestamp, self.method, self.path)


def log_worker(i, log_entry):
    """
    Process the log file details
    """
    # Only exit when the main thread ends
    while True:
        log = log_entry.get()
        logger.info('', extra=log.__dict__)
        log_entry.task_done()


def run_async_log(request, response):
    """
    Push data to queue for processing
    """
    request.log.status_code = response.status_code
    request.log.response = str(response)
    request.log.time = (now() - request.log.timestamp).total_seconds() * 1000
    timestamp_string = request.log.timestamp.strftime('%Y-%m-%d %H:%M:%S.%f')
    request.log.timestamp = timestamp_string
    request.log.size = len(response.content)

    # Push log information to queue
    log_queue.put(request.log)


class LoggingMiddleware(object):
    """
    Logs data from requests/responses
    """
    def __init__(self):
        """
        Create worker threads
        """
        for i in range(num_log_threads):
            worker = Thread(target=log_worker, args=(i, log_queue))
            worker.setDaemon(True)
            worker.start()

    def process_request(self, request):
        """
        Create log instance and process initial request information
        """
        # Don't log in debug or dev mode
        if settings.DEBUG:
            return None

        # If user agent not allowed e.g Health checker
        http_user_agent = request.environ.get('HTTP_USER_AGENT', '')
        if http_user_agent in DISALLOWED_USER_AGENTS:
            return None

        # Log all methods unless specifically set GET, POST etc
        if (not hasattr(settings, 'LOGGED_METHODS') or
           request.method in settings.LOGGED_METHODS):
            request.log = LogEntry(request)

    def process_response(self, request, response):
        """
        Run async logging task and return original response
        """
        if hasattr(request, 'log'):
            run_async_log(request, response)

        return response
